<p>¡Hola, Mundo!</p>
<?php if ($hora >= 18): ?>
  <p>Quizás debería decir buenas tardes. Ya son las <?php echo $hora ?>.</p>
<?php endif; ?>

<?php if ($sf_params->has('nombre')): ?>
  <p>¡Hola, <?php echo $sf_params->get('nombre') ?>!</p>
<?php else: ?>
  <p>¡Hola, Juan Pérez!</p>
<?php endif; ?>

<p>¡Hola, <?php echo $sf_params->get('nombre', 'Juan Pérez') ?>!</p>

<form method="post" action="<?php echo url_for('contenido/actualizar') ?>">
  <label for="nombre">¿Cómo te llamas?</label>
  <input type="text" name="nombre" id="nombre" value="" />
  <input type="submit" value="Ok" />
  <?php echo link_to('Nunca digo mi nombre', 'contenido/actualizar?nombre=anonymous') ?>
</form>
