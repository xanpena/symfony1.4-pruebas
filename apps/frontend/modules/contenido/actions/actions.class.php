<?php

/**
 * contenido actions.
 *
 * @package    MI_BLOG
 * @subpackage contenido
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class contenidoActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $this->forward('default', 'module');
  }

  /**
   * Executes index action
   *
   * @param sfRequest $request A request object
   */
  public function executeVer(sfWebRequest $request){
    $hoy = getdate();
    $this->hora = $hoy['hours'];
  }

  public function executeActualizar(sfWebRequest $request){
    $this->nombre = $request->getParameter('nombre');
    //return sfView::SUCCESS;
    return $this->renderText("<html><body>¡Hola Mundo!</body></html>");
  }
}
